import React from 'react';
import Style from './InputGroup.module.css';

interface Iprops {
  children: any
}

const InputGroup = (props: Iprops) => {
  return (
    <div className={Style.InputGroup}>
      {props.children}
    </div>
  );
}

export default InputGroup;