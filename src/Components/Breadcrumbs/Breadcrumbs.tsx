import React from 'react';
import { Link } from 'react-router-dom';

import Style from './Breadcrumbs.module.css';

export interface BreadcrumbsProps {
  list: [{link: string, title: string}];
}

const Breadcrumbs = ( props:BreadcrumbsProps ) => {
  return (
    <div className={Style.Breadcrumbs}>
    {
      props.list.map((item, index) => {
        if(index===props.list.length-1){return null}
        return <Link key={index} to={item.link}>{item.title}</Link>
      })
    }
      <span>{props.list[props.list.length-1].title}</span>
    </div>
  );
}

export default Breadcrumbs;
