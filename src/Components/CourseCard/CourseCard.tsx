import React from 'react';
import Style from './CourseCard.module.css';

interface Iprops {
  image: string
  title: string
  timeLength?: number
  buyNum?: number
  price: number
  offPrice?: number
}

const CourseCard = (props: Iprops) => {
  return (
    <div className={Style.CourseCard}>
      <div className={Style.image}>
        <img src={props.image} alt=""/>
      </div>
      <h1 className={Style.title}>{props.title}</h1>
      <p className={Style.info}></p>
      <p className={Style.price}></p>
    </div>
  );
}

export default CourseCard;
