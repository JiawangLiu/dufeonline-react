import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Style from './Navigation.module.css';

// import Modal from '../Modal/Modal';
// import Login from '../Login/Login';

interface keyword {
  keyword: string
}

interface Istate {
  keywords: Array<keyword>
  keywordsShow: boolean
  searchKey: string
  loginVisible: boolean
}

class Navigation extends Component<{}, Istate> {

  constructor(props: {}){
    super(props)
    this.state = {
      keywords : [],
      searchKey: "",
      keywordsShow: true,
      loginVisible: false,
    }
  }

  componentWillMount(){
    fetch('/getKeywords?num=3')
    .then(Response=>Response.json())
    .then(Data=>{
      this.setState({keywords: Data.keywords})
    })
  }

  closeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({loginVisible: false})
  }

  renderKeywords(){
    if(!this.state.keywordsShow || this.state.searchKey !== '') return null;
    return (
      <div className={Style.KeyWords}>
        {this.state.keywords.map(item=>{return <span onClick={()=>{this.setState({searchKey: item.keyword})}} key={item.keyword}>{item.keyword}</span>})}
      </div>
    )
  }

  render() {
    return (
      <div className={Style.Navigation}>
        <div className="container">
          <ul className={Style.Links}>
            <li className={Style.LinksItem}><Link to="/">首页</Link></li>
            <li className={Style.LinksItem}><Link to="/Course">课程中心</Link></li>
            <li className={Style.LinksItem}><a href="/">会计职称</a></li>
            <li className={Style.LinksItem}><a href="/">学习专区</a></li>
            <li className={Style.LinksItem}><a href="/">会计继续教育</a></li>
          </ul>
          <div className={Style.RightWrap}>
            <div className={Style.Search}>
              <div className={Style.Input}>
                <input
                  type="text"
                  value={this.state.searchKey}
                  onChange={(e)=>this.setState({searchKey:e.target.value})}
                  onFocus={()=>this.setState({keywordsShow: false})}
                  onBlur={()=>this.state.searchKey || this.setState({keywordsShow: true})} />
                {this.renderKeywords()}
              </div>
              <button className={Style.SearchBtn}></button>
            </div>
            <button className={`${Style.Button} ${Style.Login}`}>登录</button>
            <button className={`${Style.Button} ${Style.Register}`}>注册</button>
          </div>
        </div>
        {/* <Modal title="用户登陆" visible={this.state.loginVisible} onClose={this.closeLogin}>
          <Login />
        </Modal> */}
      </div>
    );
  }
}

export default Navigation;
