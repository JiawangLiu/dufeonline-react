import React, { PureComponent } from 'react';
import Style from './Input.module.css';

interface Iprops {
  type: string
  value: string
  placeholder: string
  onChange: (e?: React.ChangeEvent<HTMLInputElement>) => void;
}

interface Istate {
  top: boolean
}

class Input extends PureComponent<Iprops, Istate> {

  constructor(props: Iprops){
    super(props)
    this.state = {
      top: false
    }
  }

  onFocus = () => {
    this.setState({top: true})
  }

  onBlur = (e: React.ChangeEvent<HTMLInputElement>) => {
    if(!e.target.value){this.setState({top: false})}
  }


  render() {
    return (
      <div className={Style.InputGroupRow}>
        <label className={`${Style.Placeholder} ${this.state.top && Style.Top}`}>{this.props.placeholder}</label>
        <input
          type={this.props.type}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          value={this.props.value}
          onChange={this.props.onChange}
        />
      </div>
    );
  }
}

export default Input;
