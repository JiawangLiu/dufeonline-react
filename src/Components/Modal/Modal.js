import React, { Component } from 'react';
import './Modal.css';

import Dialog from 'rc-dialog';

class Modal extends Component {

  constructor(props){
    super(props)
    this.state = {
    }
  }

  handleClose = (e) => {
    const onClose = this.props.onClose;
    if (onClose) {
      onClose(e);
    }
  };

  render() {
    return (
      <Dialog
        prefixCls="Modal"
        // className="Modal"
        // wrapClassName="Modal-wrap"
        visible={this.props.visible}
        title="用户登陆"
        onClose={this.handleClose}
        // footer={footer === undefined ? defaultFooter : footer}
        // mousePosition={mousePosition}
        // onClose={this.handleCancel}
      >
        {this.props.children}
      </Dialog>
    );
  }
}

export default Modal;
