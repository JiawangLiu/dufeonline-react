import React from 'react';
import './Carousel.css';

import Slider from "react-slick";

interface CarouselItem {
  address: string,
  pic: string,
  showType: string,
  seq: number,
  vid: string
}

interface CarouselProps {
  list: Array<CarouselItem>
}

const settings = {
  arrows: false,
  dots: true,
  infinite: true,
  fade: true,
  cssEase: 'linear',
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  className: 'Carousel',
  dotsClass: 'Carousel-dots',
  customPaging: () => (<span className="Carousel-dot"></span>)
};

const Carousel = ( props: CarouselProps ) => {
    return (
      <Slider {...settings}>
        {
          props.list.map((item, index)=>{
            return (
              <a href={item.address} key={index}>
                <img src={item.pic} alt=""/>
              </a>
            )
          })
        }
      </Slider>
    );
}

export default Carousel;
