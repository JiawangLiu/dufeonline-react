import React, { Component } from 'react';
import Style from './Login.module.css';

import Input from '../Input/Input';
import InputGroup from '../InputGroup/InputGroup';

class Login extends Component {

  constructor(props){
    super(props)
    this.state = {
      account: "",
      password: "",
      errorMessage: "",
    }
  }

  changeAccount = (e) => {
    this.setState({
      account: e.target.value
    })
  }
  changePassword = (e) => {
    this.setState({
      password: e.target.value
    })
  }

  doLogin = () => {
    if(!this.state.account){
      this.setState({errorMessage: "请填写手机号！"})
      return;
    }
    if(!this.state.password){
      this.setState({errorMessage: "请填写密码！"})
      return;
    }

    fetch('login')
    .then(Response=>console.log(Response))

  }

  render() {
    return (
      <div className={Style.Login}>
        <div className={Style.Error}>{this.state.errorMessage}</div>
        <InputGroup>
          <Input type="text" placeholder="手机号" value={this.state.account} onChange={this.changeAccount}/>
          <Input type="password" placeholder="密码" value={this.state.password} onChange={this.changePassword}/>
        </InputGroup>
        <button className={Style.Button} onClick={this.doLogin}>登陆</button>
        <p className={Style.Register}>新用户，<span>快速注册</span></p>
        <p className={Style.Wechat}>其他登录方式<img src={require('./img/wechat.png')} alt=""></img></p>
      </div>
    );
  }
}

export default Login;
