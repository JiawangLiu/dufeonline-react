import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Header from 'Components/Header/Header';
import Navigation from 'Components/Navigation/Navigation';
import Footer from 'Components/Footer/Footer';

import List from './List/List';
import VideoCourse from './VideoCourse/VideoCourse';
import LiveCourse from './LiveCourse/LiveCourse';
import FaceCourse from './FaceCourse/FaceCourse';
import TrainCourse from './TrainCourse/TrainCourse';

import Breadcrumbs from 'Components/Breadcrumbs/Breadcrumbs';


class Course extends Component {

  constructor(props){
    super(props)
    this.state = {

    }
  }

  componentWillMount(){

  }

  componentDidMount(){

  }

  render() {
    return (
      <div className="Course">
        <Header />
        <Navigation />
        <div className="container"><Breadcrumbs list={[{link:'/', title: '首页'},{link:'Course', title: '课程中心'}]} /></div>
        <Router>
          <Switch>
            <Route path="/Course/Video/:CourseId" component={VideoCourse}></Route>
            <Route path="/Course/Live/:CourseId" component={LiveCourse}></Route>
            <Route path="/Course/Face/:CourseId" component={FaceCourse}></Route>
            <Route path="/Course/Train/:CourseId" component={TrainCourse}></Route>
            <Route path="/Course" component={List}></Route>
          </Switch>
        </Router>
        <Footer />
      </div>
    );
  }
}

export default Course;
