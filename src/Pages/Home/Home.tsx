import React, { Component } from 'react';

import fly from '../../Lib/request';

import Header from '../../components/Header/Header';
import Navigation from '../../components/Navigation/Navigation';
import Carousel from '../../components/Carousel/Carousel';
import Footer from '../../components/Footer/Footer';

import CourseCard from '../../components/CourseCard/CourseCard';

import './Home.css';

interface CarouselItem {
  address: string,
  pic: string,
  showType: string,
  seq: number,
  vid: string
}

export interface HomeState {
  visible: boolean;
  carouselList: Array<CarouselItem>
}

class Home extends Component<{}, HomeState> {

  constructor(props:any){
    super(props)
    this.state = {
      visible: true,
      carouselList: []
    }
  }

  componentWillMount(){
    fly.get('/getCarouselList?type=1')
    .then(res => {
      this.setState({carouselList: res.data})
      console.log(res)
    })
    // fly.get('/getStudentCourseList')
    // .then(res => {
    //   console.log(res)
    // })
  }

  componentDidMount(){

  }

  render() {
    return (
      <div className="Home">
        <Header />
        <Navigation />
        <Carousel list={this.state.carouselList}/>

        <div className="Home-CourseCard-wrap">
          <div className="Home-CourseCard-item">
            <CourseCard
              image="http://file.edufe.cn/b2c_mob_pic/static/upload/photos_course/def83840bb1f42c8b9e450bf7a982da5.jpg"
              title="2019年初级会计冲刺班·大连面授（报名即送网课）"
              price={200}/>
          </div>
        </div>

        <Footer />
      </div>
    );
  }
}

export default Home;
