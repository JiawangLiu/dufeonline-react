import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Home from './Pages/Home/Home';
// import Course from './Pages/Course/Course';

import './App.css';


class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" exact component={Home}></Route>
          {/* <Route path="/Course" component={Course}></Route> */}
        </Switch>
      </Router>
      // <div>
      //   <Route path="/" component={Home}></Route>
      // </div>
    );
  }
}

export default App;
