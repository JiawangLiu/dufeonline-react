import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './index.css';
import './Lib/Style';


ReactDOM.render(<App />, document.getElementById('root'));