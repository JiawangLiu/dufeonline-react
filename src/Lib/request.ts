import fly from 'flyio'

fly.config.headers['token'] = sessionStorage.getItem('token')

fly.interceptors.request.use((request) => {
  return request;
})

fly.interceptors.response.use(
  (response) => {
    console.log(response)
    return response
  },
  (err) => {
    console.log(err)
  }
)

export default fly